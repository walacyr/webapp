<!DOCTYPE html>
<html lang="pt-BR">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content = "width=device-width, initial-scale=1">

    <title>Clientes</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>

</head>

<style>
</style>

<body>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

    <header class="mdl-layout__header" style="margin-bottom: 20px;">

        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">Clientes</span>
        </div>

    </header>

    <main class="mdl-layout__content">

        <div>
            <table id="tableClient" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp" align="center">
                <thead>
                <tr>
                    <th>Código</th>
                    <th class="mdl-data-table__cell--non-numeric">Nome</th>
                    <th class="mdl-data-table__cell--non-numeric">Email</th>
                    <th>Tipo</th>
                    <th class="mdl-data-table__cell--non-numeric">Data de Cadastro</th>
                    <th><td><button id="editButton" onClick="showAddDialog()" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" style="background-color: #4CAF50;">Adicionar</button></td></th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <?php
                    $json_obj = file_get_contents('http://clientewalacy.gearhostpreview.com/api/Default/');
                    $json_str = json_decode($json_obj);
                    $cont = 0;
                    while($cont < sizeof($json_str)){?>
                    <td><?php echo $json_str[$cont]->cod;?></td>
                    <td class="mdl-data-table__cell--non-numeric"><?php echo $json_str[$cont]->Name;?></td>
                    <td class="mdl-data-table__cell--non-numeric"><?php echo $json_str[$cont]->Email;?></td>
                    <td><?php echo $json_str[$cont]->Type;?></td>
                    <td class="mdl-data-table__cell--non-numeric"><?php echo $json_str[$cont]->CadDate;?></td>
                    <td><button id="editButton" onClick="showEditDialog(<?php echo $json_str[$cont]->cod;?>,'<?php echo $json_str[$cont]->Name;?>','<?php echo $json_str[$cont]->Email;?>',<?php echo $json_str[$cont]->Type;?>,'<?php echo $json_str[$cont]->CadDate;?>')" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">Editar</button></td>
                    <td><button id="deleteButton" onclick="showDeleteDialog(<?php echo $json_str[$cont]->cod;?>,'<?php echo $json_str[$cont]->Name;?>')" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Excluir</button></td>
                </tr>
                <?php $cont = $cont + 1; }?>
                </tbody>
            </table>
        </div>

        <dialog id="dialogAdd" class="mdl-dialog">
            <h4 class="mdl-dialog__title">Adicionar</h4>
            <div class="mdl-dialog__content">
                <form action="#">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputAddName" class="mdl-textfield__input" type="text" pattern="[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÒÖÚÇÑ0-9 ]+">
                        <label class="mdl-textfield__label" for="inputAddName">Nome</label>
                        <span class="mdl-textfield__error">Não insira caracteres especiais!</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputAddEmail" class="mdl-textfield__input" type="text" pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$">
                        <label class="mdl-textfield__label" for="inputAddEmail">Email</label>
                        <span class="mdl-textfield__error">Insira um email válido!</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputAddType" class="mdl-textfield__input" type="number" pattern="-?[0-9]*(\.[0-9]+)?">
                        <label class="mdl-textfield__label" for="inputAddType">Tipo</label>
                        <span class="mdl-textfield__error">Tipo deve ser um número!</span>
                    </div>
                </form>
            </div>
            <div class="mdl-dialog__actions">
                <button id="buttonAdd" type="button" class="mdl-button">Adicionar</button>
                <button type="button" class="mdl-button close">Cancelar</button>
            </div>
        </dialog>

        <dialog id="dialogEdit" class="mdl-dialog">
            <h4 class="mdl-dialog__title">Editar</h4>
            <div class="mdl-dialog__content">
                <form action="#">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputEditCod" class="mdl-textfield__input" type="text" disabled pattern="-?[0-9]*(\.[0-9]+)?">
                        <label class="mdl-textfield__label" for="inputEditCod">Código</label>
                        <span class="mdl-textfield__error">Código deve ser um número!</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputEditName" class="mdl-textfield__input" type="text" pattern="[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÒÖÚÇÑ0-9 ]+">
                        <label class="mdl-textfield__label" for="inputEditName">Nome</label>
                        <span class="mdl-textfield__error">Não insira caracteres especiais!</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputEditEmail" class="mdl-textfield__input" type="text" pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$">
                        <label class="mdl-textfield__label" for="inputEditEmail">Email</label>
                        <span class="mdl-textfield__error">Insira um email válido!</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputEditType" class="mdl-textfield__input" type="number" pattern="-?[0-9]*(\.[0-9]+)?">
                        <label class="mdl-textfield__label" for="inputEditType">Tipo</label>
                        <span class="mdl-textfield__error">Tipo deve ser um número!</span>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input id="inputEditCadDate" class="mdl-textfield__input" type="text" disabled>
                        <label class="mdl-textfield__label" for="inputEditCadDate">Data de Cadastro</label>
                    </div>
                </form>
            </div>
            <div class="mdl-dialog__actions">
                <button id="buttonSave" type="button" class="mdl-button">Salvar</button>
                <button type="button" class="mdl-button close">Cancelar</button>
            </div>
        </dialog>

        <dialog id="dialogDelete" class="mdl-dialog">
            <h4 class="mdl-dialog__title">Excluir</h4>
            <div class="mdl-dialog__content">
                <p>
                    Deseja realmente excluir?
                </p>
            </div>
            <div class="mdl-dialog__actions mdl-dialog__actions--full-width">
                <button id="buttonDelete" type="button" class="mdl-button">Sim</button>
                <button type="button" class="mdl-button close">Não</button>
            </div>
        </dialog>

    </main>

</div>

<script>

    function showAddDialog(){
        var dialog = document.querySelector('#dialogAdd');
        document.getElementById('inputAddName').value = "";
        document.getElementById('inputAddEmail').value = "";
        document.getElementById('inputAddType').value = "";
        dialog.showModal();
        dialog.querySelector('#buttonAdd').onclick = function() {

            obj = new Object();
            obj.Name = document.getElementById('inputAddName').value;
            obj.Email = document.getElementById('inputAddEmail').value;
            obj.Type = document.getElementById('inputAddType').value;
            if(obj.Name!="" && obj.Type!="" && obj.Email !="")
            {
                if(validString(obj.Name))
                {
                    if(validEmail(obj.Email))
                    {
                        $.ajax({
                            url : "http://clientewalacy.gearhostpreview.com/api/Default/",
                            data : obj,
                            type : "Post",
                            sucess:function(result){
                                alert(result);
                            }
                        });
                        RefreshTable();
                        dialog.close();
                    }
                    else
                    {
                        alert("Insira um email válido!");
                    }
                }
                else
                {
                    alert("Não insira caracteres especiais no nome!");
                }
            }
            else
            {
                alert("Não deixe campos em branco!");
            }

        }
        dialog.querySelector('.close').addEventListener('click', function() {
            dialog.close();
        });
    }

    function styleInputs() {
        $("#inputEditCod").parent('.mdl-textfield').removeClass('is-invalid').addClass('is-dirty');
        $("#inputEditName").parent('.mdl-textfield').removeClass('is-invalid').addClass('is-dirty');
        $("#inputEditEmail").parent('.mdl-textfield').removeClass('is-invalid').addClass('is-dirty');
        $("#inputEditType").parent('.mdl-textfield').removeClass('is-invalid').addClass('is-dirty');
        $("#inputEditCadDate").parent('.mdl-textfield').removeClass('is-invalid').addClass('is-dirty');
    }
    function showEditDialog(cod, name, email, type, date) {
        var dialog = document.querySelector('#dialogEdit');
        dialog.showModal();
        $('#inputEditCod').val(cod);
        $('#inputEditName').val(name);
        $('#inputEditEmail').val(email);
        $('#inputEditType').val(type);
        $('#inputEditCadDate').val(date);
        styleInputs();
        dialog.querySelector('#buttonSave').onclick = function() {

            obj = new Object();
            obj.Name = document.getElementById('inputEditName').value;
            obj.Email = document.getElementById('inputEditEmail').value;
            obj.Type = document.getElementById('inputEditType').value;
            if(obj.Name!="" && obj.Type!="" && obj.Email!="") {
                if(validString(obj.Name))
                {
                    if(validEmail(obj.Email))
                    {
                        $.ajax({
                            url: "http://clientewalacy.gearhostpreview.com/api/Default/" + cod,
                            data: obj,
                            type: "Put",
                            sucess: function (result) {
                                alert(result);
                            }
                        });
                        RefreshTable();
                        dialog.close();
                    }
                    else
                    {
                        alert("Insira um email válido!");
                    }
                }
                else
                {
                    alert("Não insira caracteres especiais no nome!");
                }
            }
            else
            {
                alert("Não deixe campos em branco!");
            }
        }
        dialog.querySelector('.close').addEventListener('click', function() {
            dialog.close();
        });

    }

    function showDeleteDialog(cod, name){
        var dialog = document.querySelector('#dialogDelete');
        dialog.showModal();
        dialog.querySelector('#buttonDelete').onclick = function() {
            $.ajax({
                url : "http://clientewalacy.gearhostpreview.com/api/Default/"+cod,
                type : "Delete",
                sucess:function(result){
                    alert(result);
                }
            });
            RefreshTable();
            dialog.close();
        }
        dialog.querySelector('.close').addEventListener('click', function() {
            dialog.close();
        });
    }

    function RefreshTable() {
        $( "#tableClient" ).load( "index.php #tableClient" );
    }

    function validString(value){
        if (/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÒÖÚÇÑ0-9 ]+$/.test(value)) {
            return true;
        }
    }

    function validEmail(value){
        if (/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(value)) {
            return true;
        }
    }

</script>
</body>
</html>